======================================
SQL ADVANCE SELECTS and JOINING TABLES
======================================


---Taylor swift
INSERT INTO artist(name)
VALUES
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");

INSERT INTO albums(album_title,date_released, artist_id)
VALUES 
("Fearless","2008-1-1",5),
("Red","2012-1-1",5);



INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Fearless",246,"Pop Rock",5),
("Love Story",213,"Country Pop",5),
("State of Grace",313,"Rock, Alternative Rock, Arena Rock",6),
("Red",204,"Country",6);



-----> lady gaga
INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("A Star Is Born","2018-1-1",6),
("Born This Way","2011-1-1",6);



INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Black Eyes",221,"Rock and Roll",7),
("Shallow",201,"Country, Rock, Folk Rock",7),
("Born This Way",252,"Electropop",8);



-----> justin
INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("Purpose","2015-1-1",7),
("Believe","2012-1-1",7);


INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Sorry",232,"Dancehall-poptropical, housemoombahton",9),
("Boyfriend",251,"Pop",10);



---> ariana

INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("Dangerous Woman","2016-1-1",8),
("Thank U, Next","2019-1-1",8);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Into You",242,"EDM House",11),
("Thank U, Next",236,"Pop, R&B",12);



----->> bruno
INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("24K Magic","2016-1-1",9),
("Earth to Mars","2011-1-1",9);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("24K Magic",207,"Funk Disco, R&B",13),
("Lost",232,"Pop",14);




======== EXCLUDE RECORDS ========
Syntax:
	SELECT column_name FROM table_name WHERE column_name != value;

Example:
	SELECT * FROM songs WHERE id != 11;

multiple:
	SELECT * FROM songs WHERE id != 7 AND album_id != 8;



======= FINDING RECORDS (COMPARISON) =======
Example:
	SELECT * FROM songs WHERE length > 230;
	SELECT * FROM songs WHERE length < 200;
	SELECT * FROM songs WHERE genre = "Pop" OR genre = "Pop rock";
	SELECT * FROM songs WHERE length > 230 OR length <200;



====RECORDS WITH SPECIFIC CONDITIONS ========
Example:
	SELECT * FROM songs WHERE id = 5 OR id = 6 OR id = 7;

	OR

	(Using IN clause):

	SELECT * FROM songs WHERE id IN (5,6,7);
	SELECT * FROM songs WHERE genre IN ("Pop", "Electropop", "EDM House");



===== RECORDS WITH PARTIAL MATCH =====

	(LIKE CLAUSE)

	Ex:
	(match at start)
		SELECT * FROM songs WHERE song_name LIKE "th%";

	(match at end)
		SELECT * FROM songs WHERE song_name LIKE "%ce";

	(any position)
		SELECT * FROM songs WHERE song_name LIKE "%or%";

	(match of specific length or pattern)
		SELECT * FROM songs WHERE song_name LIKE "__r_";
		SELECT * FROM songs WHERE song_name LIKE "__v_%";
		SELECT * FROM albums WHERE album_title LIKE "_ur%";



======= SORT RECORDS ========
Syntax:
	SELECT * FROM songs ORDER BY song_name;
	SELECT * FROM songs ORDER BY song_name ASC;
	SELECT * FROM songs ORDER BY length DESC;



========== LIMITING RECORDS ======
Syntax:
	SELECT * FROM songs LIMIT 5;



======= SHOW RECORDS WITH DISTINCT VALUES =======
Ex:
	SELECT genre FROM songs;
	SELECT DISTINCT genre FROM songs;



====== JOINING 2 TABLES =======
Ex:
	SELECT * FROM artist 
	JOIN albums ON artist.id = albums.artist_id;

	multiple:
	SELECT * FROM artist 
	JOIN albums ON artist.id = albums.artist_id 
	JOIN songs ON albums.id = songs.album_id;

	
====== JOINING TABLES w/ SPECIFIED WHERE CONDITIONS =======
Ex:
	SELECT * FROM artist 
	JOIN albums ON artist.id = albums.artist_id WHERE name LIKE "%a%";


======= SELECTING COLUMNS ==========
Ex:
	SELECT name,album_title,date_released,song_name,length, genre FROM artist
	JOIN albums ON artist.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;


===== PROVIDING ALIASES FOR JOINING TABLE =====
Ex:
	SELECT name AS band, album_title AS album, date_released, song_name AS song, length, genre FROM artist
	JOIN albums ON artist.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id



====== DISPLAYING DATA FROM JOING TABLES =====

INSERT INTO users (username,password,full_name,contact_number, email, address)
VALUES ("Theooon","pw1","Theo lique", 1123456789, "theon@gmail.com", "Luzon");

INSERT INTO playlists(user_id, datetime_created)
VALUES (1,"2022-09-20 01:00:00");

INSERT INTO playlists_songs (playlist_id, song_id)
VALUES (1, 8), (1,14), (1,10);

JOINING MULTIPLE TABLES:

SELECT * FROM playlists 
JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
JOIN songs ON playlists_songs.song_id = songs.id;


SELECT user_id, datetime_created, song_name, length, genre, album_id
FROM playlists 
JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
JOIN songs ON playlists_songs.song_id = songs.id;












